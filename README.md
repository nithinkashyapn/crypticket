#Crypticket

## API endpoints

+ Get copy of Blockchain - `/blocks`

+ Mine a block in the Blockchain - `/mine`

## Start the server

`npm run dev`